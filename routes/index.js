var express = require('express');
var router = express.Router();
const axios = require('axios');

router.get('/', function(req, res, next) {
  axios.get("http://data.fixer.io/api/latest?access_key=fe3a9b3f9ac173a3e51d8ea22951cf95")
  .then((response) => {
    const fx = response.data.rates;
    res.render('fx', {fxs: fx});
  })
  .catch((err) => {
    console.log(err);
  });
});

module.exports = router;
